Amanda Ward
===========

----

> Reliable Registered Nurse and Midwife with over 18 years experience in a variety of settings

> <amanda@amandaward.work> • +61 417009648 •
> Brisbane, Australia

----

Professional Experience
-----------------------

April 2014-*present*
:   **Donor Coordinator/Nurse Coordinator**
:   Queensland Fertility Group

Role includes working as a Registered Nurse and Midwife within the Donor program.

Key duties include:

* Patient education regarding normal menstrual cycles and stimulated cycles
* Medication education
* Telephone consultations
* Recruiting and assessing potential semen donors
* Maintaining adequate donors for patients to select. Involves local donors and importing donors from the USA
* Meticulous documentation and data management
* Liaising with donor conceived persons
* Appointments with patients undergoing any of the following cycles - Known Oocyte Donation, Anonymous Oocyte Donation, Known Embryo Donation, Anonymous Embryo Donation, Known Semen Donation, Anonymous Semen Donation, Surrogacy
* Working within the NHMRC and RTAC guidelines for Gamete Donation


May 2013-May 2014
:   **Nurse Concierge**
:   Medibank Health Solutions

This role was created to improve staff retention within the nursing team at Medibank Health Solutions. It involved redesigning the interview process, developing ideas for staff engagement and retention and management of data regarding staff employment, satisfaction and retention. I was responsible for interviewing and employing all nursing staff for Medibank Health Solutions in the role of Telephone Triage Nurse. This involved conducting remote interviews with nurses throughout Australia. I was also responsible for coordination of staff engagement strategies and state team building exercises.

August 2012-May 2013
:   **Team Leader**
:   Medibank Health Solutions

This role entailed working from home managing a team of up to 15 nurses remotely throughout Australia. It involved reviewing their calls, giving feedback for improvement, maintaining remote team engagment and education.

April 2011-August 2012
:   **Telephone Triage Nurse**
:   Medibank Health Solutions

This role entailed working from home taking calls from the general public throughout Australia. Triaging of callers involved accurate assessment and questioning to arrive at a suitable recommendation for them. The role involved liaising with phone GP's, remote nursing posts, police, fire and ambulance services throughout Australia.

2008-April 2011
:   **Clinical Midwife**
:   Royal Brisbane and Women's Hospital

Worked on both the Postnatal Unit and Birth Suite as a Clinical Midwife.

Key roles include:

* Management and supervision of student midwives
* Education of staff
* Looking after complex cases both in Birth Suite and on the Postnatal Unit
* Reviewing learning objectives and professional development portfolios of staff


2008-April 2011
:   **Nurse Manager Women's Health (Patient Flow)**
:   Royal Brisbane and Women's Hospital

This role involved the coordination of staff, admissions and discharges for the Gynaecology Unit, Antental Unit, Birth Suite, Postnatal Unit, Special Care Nursing and Neonatal Intensive Care Unit.

Key roles include:


* Ensuring each ward was sufficiently staffed for the following 24 hours and if not organising replacement staff through the casual pool and/or agencies whilst working within current budget constraints
* Ensuring timely discharges of appropriate patients so that the timely flow from Emergency Depatment and Theatres could be maintained
* Attending meetings twice per shift with other bed managers obtain a current hospital status and share updates
* Liaising with Retrieval Services Queensland to accept patients in from other Queensland hospitals who require tertiary care
* Disaster Management - Coordination of patients and staffing during Cyclone Yasi in 2011

2007-2008
:   **Registered Midwife (Team Midwifery)**
:   Royal Brisbane and Women's Hospital

Worked as part of a Midwifery team consisting of 5 midwives that focused on a woman-centred continuity of care model for antenatal, birth and postpartum. The philosophy of the team was to care for the same women during their antenatal, intrapartum and postpartum period and therefore ensuring continuity of care, resulting in increased satisfaction for both patients and staff.

2006-2007
:   **Registered Midwife**
:   Royal Brisbane and Women's Hospital

Rotated through all areas of the Maternity Unit including;

* Antenatal Clinic
* ANDAS (Antental Day Assessment Service)
* Birth Suite
* Special Care Nursery
* Postnatal Unit


2004-2006
:   **Registered Midwife**
:   Gold Coast Hospital

Worked on all wards within the Maternity Unit including Antenatal Clinic, Birth Suite and Postnatal Unit. Gained experience in all aspects of Antenatal Clinic including assessment, diagnostic test ordering and interpretation, follow up, education of patients and procedure management. Increased Birth Suite experience in management of labour and delivery of normal births, vacuum births, forceps delivery and emergency caesarean sections. The Postnatal Unit increased my experience in post birth care and readmission care of mothers and babies.

2002-2004
:   **Registered Nurse**
:   Gold Coast Hospital

Worked on the casual pool in all wards of the hospital including:

* Emergency Department
* Medical Admissions Unit
* Orthopaedics
* Paediatrics
* Mental Health Unit
* Neurology
* General Surgical
* Cardiology
* Urology


Education
---------

2021
:   National Immunisation Education for Health Professionals, Australian Catholic University

2004
:   Graduate Diploma in Midwifery, Queensland University of Technology

2001
:   Bachelor of Nursing, Australian Catholic University

1998
:   Senior Certificate, St Margaret's Anglican Girls School

References
----------

*available upon request*

----

> <amanda@amandaward.work> • +61 417009648 •
> Brisbane, Australia
